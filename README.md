# netr

Netr is a mobile App written in flutter for my personal use, to monitor my CCTV cameras both over intranet and internet (using SSH). It is not available at any app store. If you want to use it, build and deploy to your mobile.

## Features
- Supports Android/Ios
- Supports multiple CCTV streams both over local intranet and internet (using SSH).
- Supports watching CCTV streams using VLC remote web interface.
- Supports update over OTA from your local intranet (or apk hosted on some URL).

## Configuration
All configuration information is stored in `lib/config.dart`. Edit it to your liking before build App.

## Building App
To build app use `build.sh`. To build check steps in this shell script.

# Screenshots

## Main screen

![Main Screen](https://gitlab.com/slashblog/netr-app/-/raw/main/screenshots/main.png?inline=false "Main Screen")

## Live video screen
![Live Video Screen](https://gitlab.com/slashblog/netr-app/-/raw/main/screenshots/video.png?inline=false "Live Video Screen")

## Upgrade screen
![Upgrade Screen](https://gitlab.com/slashblog/netr-app/-/raw/main/screenshots/upgrade.png?inline=false "Upgrade Screen")
